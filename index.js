var express = require('express');
var Shipment = require('./controller/shipment-backend');
var app = express();

//body reader
var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();

/* 
//Get shipment quote
 */
app.use((request, response, next) => {
	response.setHeader('Access-Control-Allow-Origin', '*');
	response.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
	response.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");

	next();
});


app.post('/client/getquote', jsonParser, (req, res) => {
  // console.log(12);
  Shipment.getQuote(req, res);
});

app.get('/', (request, response) => {
	response.send({response: 'API response'});
});

/* 
Create server
 */
app.listen(8081, () => {
  console.log("Example app listening on port 8081");
})
