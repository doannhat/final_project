const MongoClient = require('mongodb').MongoClient;
const url = "mongodb://localhost:27017/final-project-db";
const fs = require('fs');

exports.getQuote = (req, res) => {
    // res.header("Access-Control-Allow-Origin", "*");

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    // res.setHeader('Access-Control-Allow-Credentials', true);
    MongoClient.connect(url, (err, db) => {
        let shipmentBody = req.body.data;
        if (err) throw new Error(err);
        console.log("Database connected.");

        //connect to database
        let dbo = db.db('final-project-db');

        //check country's validation
        let destinationCountry = shipmentBody.destination.address.country_code;
        let originCountry = shipmentBody.origin.address.country_code;
        if (destinationCountry !== "FR" || originCountry !== "FR") {
            console.log("Our service supports shipments in France only.")
        }

        //get rate info
        let package = shipmentBody.package;
        let w = 0;
        if (package.grossWeight.unit === "kg") {
            console.log("KG");
            w = package.grossWeight.amount * 1000;
        } else {
            console.log("G");
            w = package.grossWeight.amount;
        }
        if (w>15000) {
            w=15100;
        }
        rate = dbo.collection('rate').findOne({
            "weight_maxi": { $gte: w }
        }, ['_id', 'price'], (err, data) => {
            if (err) throw new Error(err);
            // console.log(res);
            // if (data) {
            //     resp = [
            //         {
            //             "quote_id": data._id,
            //             "amount": data.price //EUR
            //         }
            //     ];
            //     res.json(_ResponseData(resp));
            // } else {
            //     resp = [
            //         {
            //             "quote_id": "5d3ebfc29e53cf08d86c5d8e",
            //             "amount": 100 //EUR
            //         }
            //     ];
            //     // res.header("Access-Control-Allow-Origin", "*");
            //     res.json(_ResponseData(resp));
            // }
            resp = [
                {
                    "quote_id": data._id,
                    "amount": data.price //EUR
                }
            ];
            res.json(_ResponseData(resp));

        });
        db.close();
    })
}

function _ResponseData(data = {}) {
    return {
        "data": data ? data : {},
    }
}